function ejercicio02() {
    function areaTriangulo(h,b) {
        return (h*b)/2;
    }
    var h = 5;
    var b = 10;
    var x = areaTriangulo(h,b);
    console.log(x);

    function perimetroTriangulo(a,b,c) {
        return a+b+c;
    }
    var a = 4;
    var b = 10;
    var c = 6
    var y = perimetroTriangulo(a,b,c);
    console.log(y);
}
ejercicio02();