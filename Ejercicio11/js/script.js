function ejercicio11() {
    function areaSectorCircular(n,R,pi) {
        return (pi*R**2*n)/360;
    }
    var n = 45;
    var R = 20;
    var pi = Math.PI;
    var x = areaSectorCircular(n,R,pi);
    console.log(x);
}
ejercicio11();