function ejercicio24() {
    function areaOctaedro(a,n) {
        return 2*n*(a**2);
    }
    var a = 10;
    var n = Math.sqrt(3);
    var x = areaOctaedro(a,n);
    console.log(x);

    function volumenOctaedro(a,m) {
        return (m*(a*a*a))/3;
    }
    var a = 10;
    var m = Math.sqrt(2);
    var y = volumenOctaedro(a,m);
    console.log(y);
}
ejercicio24();