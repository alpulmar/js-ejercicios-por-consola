function ejercicio20() {
    function areaOctoedro(a,b,c) {
        return (2*(a*b))+(2*(b*c));
    }
    var a = 50;
    var b = 10;
    var c = 10;
    var x = areaOctoedro(a,b,c);
    console.log(x);

    function volumenOctoedro(a,b,c) {
        return a*b*c;
    }
    var a = 50;
    var b = 10;
    var c = 10;
    var y = volumenOctoedro(a,b,c);
    console.log(y);
}
ejercicio20();