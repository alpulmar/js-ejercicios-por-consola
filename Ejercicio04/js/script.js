function ejercicio04() {
    function areaParalelogramo(h,b) {
        return h*b;
    }
    var h = 10;
    var b = 50;
    var x = areaParalelogramo(h,b);
    console.log(x);

    function perimetroParalelogramo(a,b) {
        return (a+b)*2;
    }
    var a = 10
    var b = 50;
    var y = perimetroParalelogramo(a,b);
    console.log(y);
}
ejercicio04();