function ejercicio13() {
    function areaCono(R,pi,g) {
        return pi*R*(R+g);
    }
    var R = 15;
    var g = 50;
    var pi = Math.PI;
    var x = areaCono(R,pi,g);
    console.log(x);

    function volumenCono(R,pi,h) {
        return (pi*R**2*h)/3;
    }
    var R = 15;
    var h = 40;
    var pi = Math.PI;
    var y = volumenCono(R,pi,h);
    console.log(y);
}
ejercicio13();