function ejercicio19() {
    function areaCubo(a) {
        return (a**2)*6;
    }
    var a = 10;
    var x = areaCubo(a);
    console.log(x);

    function volumenCubo(a) {
        return a*a*a;
    }
    var a = 10;
    var y = volumenCubo(a);
    console.log(y);
}
ejercicio19();