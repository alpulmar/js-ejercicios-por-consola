function ejercicio21() {
    function areaPrisma(a,h,P) {
        return (a+h)*P;
    }
    var a = 10;
    var h = 25;
    var P = 50;
    var x = areaPrisma(a,h,P);
    console.log(x);

    function volumenPrisma(A,h) {
        return A*h;
    }
    var A = 250;
    var h = 25;
    var y = volumenPrisma(A,h);
    console.log(y);
}
ejercicio21();