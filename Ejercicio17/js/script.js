function ejercicio17() {
    function areaCuñaEsferica(R,pi,n) {
        return (4*pi*R**2*n)/3;
    }
    var R = 100;
    var n = 20;
    var pi = Math.PI;
    var x = areaCuñaEsferica(R,pi,n);
    console.log(x);

    function volumenCuñaEsferica(R,pi,n) {
        return (4*pi*R*R*R*n)/(3*360);
    }
    var R = 100;
    var n = 20;
    var pi = Math.PI;
    var y = volumenCuñaEsferica(R,pi,n);
    console.log(y);
}
ejercicio17();