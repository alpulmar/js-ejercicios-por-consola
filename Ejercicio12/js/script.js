function ejercicio12() {
    function areaCilindro(R,pi,h) {
        return 2*pi*R*(h+R);
    }
    var R = 50;
    var h = 15;
    var pi = Math.PI;
    var x = areaCilindro(R,pi,h);
    console.log(x);

    function volumenCilindro(R,pi,h) {
        return pi*R**2*h;
    }
    var R = 50;
    var h = 15;
    var pi = Math.PI;
    var y = volumenCilindro(R,pi,h);
    console.log(y);
}
ejercicio12();