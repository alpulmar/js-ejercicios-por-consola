function ejercicio25() {
    function areaTroncoPiramide(a,P,P2,A,A2) {
        return (P+P2)/2*a+A+A2;
    }
    var a = 60;
    var P = 120;
    var P2 = 100;
    var A = 900;
    var A2 = 625;
    var x = areaTroncoPiramide(a,P,P2,A,A2);
    console.log(x);

    function volumenTroncoPiramide(A,A2,h,raiz) {
        return ((A+A2+raiz)*h)/3;
    }
    var h = 50;
    var A = 900;
    var A2 = 625;
    var raiz = Math.sqrt(A*A2);
    var y = volumenTroncoPiramide(A,A2,h,raiz);
    console.log(y);
}
ejercicio25();