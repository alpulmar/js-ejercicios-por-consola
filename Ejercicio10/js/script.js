function ejercicio10() {
    function areaCorona(r,R,pi) {
        return pi*(R**2-r**2);
    }
    var r = 15;
    var R = 20;
    var pi = Math.PI;
    var x = areaCorona(r,R,pi);
    console.log(x);
}
ejercicio10();