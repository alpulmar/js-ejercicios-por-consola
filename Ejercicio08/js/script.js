function ejercicio08() {
    function areaHexagono(a,b) {
        return (a*b)/2;
    }
    var a = 40;
    var b = 50;
    var x = areaHexagono(a,b);
    console.log(x);

    function perimetroHexagono(n,b) {
        return n*b;
    }
    var n = 6;
    var b = 50;
    var y = perimetroHexagono(n,b);
    console.log(y);
}
ejercicio08();