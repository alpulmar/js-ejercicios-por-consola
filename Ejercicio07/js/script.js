function ejercicio07() {
    function areaTrapecio(B,b,h) {
        return ((B+b)*h)/2;
    }
    var B = 50;
    var b = 30;
    var h = 20;
    var x = areaTrapecio(B,b,h);
    console.log(x);

    function perimetroTrapecio(a,b,c,B) {
        return a+b+c+B;
    }
    var a = 35;
    var B = 50;
    var b = 30;
    var c = 35;
    var y = perimetroTrapecio(a,b,c,B);
    console.log(y);
}
ejercicio07();