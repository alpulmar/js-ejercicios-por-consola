function ejercicio05() {
    function areaRombo(D,d) {
        return (D*d)/2;
    }
    var d = 20;
    var D = 50;
    var x = areaRombo(D,d);
    console.log(x);

    function perimetroRombo(a) {
        return a*4;
    }
    var a = 60;
    var y = perimetroRombo(a);
    console.log(y);
}
ejercicio05();