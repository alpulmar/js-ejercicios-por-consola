function ejercicio09() {
    function areaCirculo(r,pi) {
        return pi*r**2;
    }
    var r = 15;
    var pi = Math.PI;
    var x = areaCirculo(r,pi);
    console.log(x);

    function perimetroCirculo(r,pi) {
        return 2*pi*r;
    }
    var r = 15;
    var pi = Math.PI;
    var y = perimetroCirculo(r,pi);
    console.log(y);
}
ejercicio09();