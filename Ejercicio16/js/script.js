function ejercicio16() {
    function areaCasqueteEsferico(R,pi,h) {
        return 2*pi*R*h;
    }
    var R = 100;
    var h = 20;
    var pi = Math.PI;
    var x = areaCasqueteEsferico(R,pi,h);
    console.log(x);

    function volumenCasqueteEsferico(R,pi,h) {
        return (pi*h**2*(3*R-h))/3;
    }
    var R = 100;
    var h = 20;
    var pi = Math.PI;
    var y = volumenCasqueteEsferico(R,pi,h);
    console.log(y);
}
ejercicio16();