function ejercicio23() {
    function areaTetraedro(a,n) {
        return (a**2)*n;
    }
    var a = 10;
    var n = Math.sqrt(3);
    var x = areaTetraedro(a,n);
    console.log(x);

    function volumenTetraedro(a,m) {
        return ((a*a*a)*m)/12;
    }
    var a = 10;
    var m = Math.sqrt(2);
    var y = volumenTetraedro(a,m);
    console.log(y);
}
ejercicio23();