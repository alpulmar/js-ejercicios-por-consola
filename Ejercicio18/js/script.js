function ejercicio18() {
    function areaCuñaEsferica(R,pi,h) {
        return 2*pi*R*h;
    }
    var R = 50;
    var h = 20;
    var pi = Math.PI;
    var x = areaCuñaEsferica(R,pi,h);
    console.log(x);

    function volumenCuñaEsferica(r,r2,pi,h) {
        return (pi*h*(h**2+3*r**2+3*r2**2))/6;
    }
    var r = 30;
    var r2 = 15;
    var h = 5;
    var pi = Math.PI;
    var y = volumenCuñaEsferica(r,r2,pi,h);
    console.log(y);
}
ejercicio18();