function ejercicio22() {
    function areaPiramide(a,a2,P) {
        return (P*(a+a2))/2;
    }
    var a = 10;
    var a2 = 35;
    var P = 80;
    var x = areaPiramide(a,a2,P);
    console.log(x);

    function volumenPiramide(A,h) {
        return (A*h)/3;
    }
    var A = 400;
    var h = 30;
    var y = volumenPiramide(A,h);
    console.log(y);
}
ejercicio22();