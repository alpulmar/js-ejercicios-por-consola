function ejercicio06() {
    function areaCometa(D,d) {
        return D*d;
    }
    var d = 40;
    var D = 100;
    var x = areaCometa(D,d);
    console.log(x);

    function perimetroCometa(a,b) {
        return (a+b)*2;
    }
    var a = 20;
    var b = 60;
    var y = perimetroCometa(a,b);
    console.log(y);
}
ejercicio06();